FROM ruby:2.7-alpine

RUN apk add --no-cache --update \
  git \
  build-base \
  openjdk11-jre

ENV APP_PATH /var/apps/extractor
ENV RACK_ENV production

WORKDIR $APP_PATH
ADD Gemfile $APP_PATH
ADD Gemfile.lock $APP_PATH
RUN bundle install

COPY . $APP_PATH

EXPOSE 9292

CMD ["bundle", "exec", "puma"]
