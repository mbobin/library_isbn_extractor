# frozen_string_literal: true

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib', 'api'))
$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib', 'app'))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'boot'

Bundler.require :default, ENV['RACK_ENV']

Dir[File.expand_path('../lib/api/*.rb', __dir__)].sort.each do |f|
  require f
end

require 'api'
require 'library_isbn_app'
