# frozen_string_literal: true

require 'spec_helper'

RSpec.describe API::Extractor do
  include Rack::Test::Methods

  def app
    described_class
  end


  before do
    post '/api/v1/extract', book: file
  end

  describe 'POST /api/v1/extract' do
    context 'with epub files' do
      let(:file) do
        Rack::Test::UploadedFile.new('spec/fixtures/book.epub', 'application/epub+zip', true)
      end

      it 'returns book info' do
        expect(last_response.status).to eq(201)

        expect(JSON.parse(last_response.body)).to match('isbn' => '9789811542756')
      end
    end

    context 'with txt files' do
      let(:file) do
        Rack::Test::UploadedFile.new('spec/fixtures/book.doc', 'application/msword', true)
      end

      it 'returns book info' do
        expect(last_response.status).to eq(201)

        expect(JSON.parse(last_response.body)).to match('isbn' => '9789811542763')
      end
    end
  end
end
