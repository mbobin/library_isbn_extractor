# frozen_string_literal: true

require 'spec_helper'

RSpec.describe API::Ping do
  include Rack::Test::Methods

  def app
    described_class
  end

  context 'GET /api/v1/ping' do
    it 'returns a pong response' do
      get '/api/v1/ping'

      expect(last_response.status).to eq(200)
    end
  end
end
