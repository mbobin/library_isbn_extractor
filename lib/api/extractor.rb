# frozen_string_literal: true

module API
  class Extractor < Grape::API
    class Extractor
      def initialize(file)
        @file = file
      end

      def execute
        ruby_isbn || tika_isbn
      end

      private

      def ruby_isbn
        ISBNExtractor::Directory::PureRubyDirectory.isbn(@file.path) rescue nil
      end

      def tika_isbn
        ISBNExtractor::Directory::TikaDirectory.isbn(@file.path) rescue nil
      end
    end

    params do
      requires :book, type: File
    end
    post '/extract' do
      data = Extractor.new(params[:book][:tempfile]).execute

      { isbn: data }
    end
  end
end
