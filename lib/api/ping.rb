# frozen_string_literal: true

module API
  class Ping < Grape::API
    get '/ping' do
      'pong'
    end
  end
end
