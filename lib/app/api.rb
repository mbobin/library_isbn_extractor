# frozen_string_literal: true
require 'grape_logging'

module LibraryIsbn
  class API < Grape::API
    logger.formatter = GrapeLogging::Formatters::Default.new
    use GrapeLogging::Middleware::RequestLogger, { logger: logger }

    prefix 'api'
    format :json
    version 'v1', vendor: 'isbn_extractor'

    mount ::API::Ping
    mount ::API::Extractor
  end
end
