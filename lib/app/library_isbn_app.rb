# frozen_string_literal: true

module LibraryIsbn
  class App
    def self.instance
      @instance ||= Rack::Builder.new do
        use Rack::Cors do
          allow do
            origins '*'
            resource '*', headers: :any, methods: :get
          end
        end

        run LibraryIsbn::App.new
      end.to_app
    end

    def call(env)
      LibraryIsbn::API.call(env)
    end
  end
end
